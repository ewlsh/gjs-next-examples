# GJS Next Examples

Using:
- Set $GJS to your GJS binary (probably "jhbuild run gjs")
- Run the correct script.

GJS Version differences:
- esm depends on rockon999/es6-modules
- importmaps depends on rockon999/import-maps