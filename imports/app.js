#!/usr/bin/gjs

imports.searchPath.push('./');
const { Application } = imports.window;

//run the application
let app = new Application();

app.application.run(ARGV);